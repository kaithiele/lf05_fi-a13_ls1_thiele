import java.util.Scanner;
class Widerstandsnetzwerke {
 
	public static void main(String[] args) {
 
		Scanner tastatur = new Scanner(System.in);

		double widerstand1, widerstand2, widerstand3;
		double parallelwiderstand23, gesamtwiderstand;
 	
 
		System.out.print("Widerstand 1 (Ohm): ");
		widerstand1 = tastatur.nextDouble();
		
		System.out.print("Widerstand 2 (Ohm): ");
		widerstand2 = tastatur.nextDouble();
		
		System.out.print("Widerstand 3 (Ohm): ");
		widerstand3 = tastatur.nextDouble();
		
		parallelwiderstand23 = berechneParallelschaltung(widerstand2, widerstand3);
		gesamtwiderstand = berechneReihenschaltung(widerstand1, parallelwiderstand23);
		
		System.out.println("\nDer Gesamtwiderstand betr�gt " + gesamtwiderstand + " Ohm.");
		System.out.println("Der Parallelwiderstand betr�gt " + parallelwiderstand23 + " Ohm.");
 }
	
 static double berechneReihenschaltung(double r1, double r2) {
	 	
	 	double rges;
	 	rges = r1 + r2;
	 	return rges;
 }
 
 static double berechneParallelschaltung(double r1, double r2) {
 
	 	double rges;
	 	rges = 1.0 / (1.0 / r1 + 1.0 / r2);
	 	return rges;
 }
}