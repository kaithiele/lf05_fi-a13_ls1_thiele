import java.util.Scanner;

public class Leistung {
	public static void main(String[] args) {
		 Scanner myScanner = new Scanner(System.in);
		 
		 double einzelleistung;
		 double einzelstromstaerke;
		 double gesamtleistung;
		 double gesamtstromstaerke;
		 final double netzspannung = 230.0;
		 final double maxStromstaerke = 16.0;
		 int anzahlPCs;
		 int anzahlStromkreise;
		 
		 
		 System.out.print("\nLeistung eines PC-Arbeitsplatzes [in Watt]: ");
		 einzelleistung = myScanner.nextDouble();
		 
		 System.out.print("Stromst�rke eines PC-Arbeitsplatzes [in Ampere]: ");
		 einzelstromstaerke = myScanner.nextDouble();
		 
		 System.out.print("Anzahl der PC-Arbeitspl�tze: ");
		 anzahlPCs = myScanner.nextInt();
		 
		 // Berechnung der erforderlichen Stromst�rke und der
		 // Anzahl der ben�tigten Stromkreise:
		 
		 gesamtleistung = einzelleistung * anzahlPCs;
		 gesamtstromstaerke = einzelstromstaerke * anzahlPCs;
		 
		 // Berechnungszeit eingebaut
	       // -----------------
	       System.out.println("\nWird berechnet!");
	       for (int i = 0; i < 14; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(150);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
		 
		 System.out.printf("\nGesamtleistung [in Watt]: %.2f\n", + gesamtleistung);
		 System.out.printf("Gesamtstromst�rke [in Ampere]: %.2f\n", + gesamtstromstaerke);
	 /*	 System.out.println("Anzahl der Stromkreise: " + anzahlStromkreise); */
		 
		 
		 
 }
}