import java.util.Scanner;

public class PCHaendler {
    
    public static void main(String[] args) {
        
        
        String artikel= liesString("Was möchten Sie bestellen?");

        
        int anzahl = liesInt("Geben Sie die Anzahl ein:");
    
        
        
        double preis = liesDouble("Geben sie den Netto Preis ein");
        

        double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
        
        
        // Verarbeiten
        
        double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
        
        double bruttogesamtpreis = berechnungGesamtbruttopreis(nettogesamtpreis , mwst );

        // Ausgeben
        
        rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
        
        

    }
    
    public static String liesString(String text) {
        Scanner myScanner = new Scanner(System.in);
        System.out.println(text);
        String ausgabe = myScanner.next();
        return ausgabe;
    }
    
    public static int liesInt(String text) {
        Scanner myScanner =new Scanner(System.in);
        System.out.println(text);
        int ausgabe = myScanner.nextInt();
        return ausgabe;
    }
    
    public static double liesDouble(String text) {
        Scanner myScanner = new Scanner(System.in);
        System.out.println(text);
        double ausgabe = myScanner.nextDouble();
        return ausgabe;
    }
    
    public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
        return anzahl * nettopreis; 
        
    }
    public static double berechnungGesamtbruttopreis(double nettogesamtpreis, double mwst) {
        return nettogesamtpreis * (1 + mwst / 100);
    
    }
    public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
        System.out.println("\tRechnung");
        System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
        System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
    }
}