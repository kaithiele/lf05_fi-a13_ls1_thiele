import java.util.Scanner; // Import der Klasse Scanner
public class Aufgabe2 {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Willkommen. Bitte geben Sie Ihren Namen an: ");
		String name = myScanner.next();
		
		System.out.println("Bitte geben Sie Ihr Alter an: ");
		String alter = myScanner.next();
		
		System.out.println("\nIhr Name lautet: " + name);
		System.out.println("Ihr Alter lautet: " + alter);
		
		myScanner.close();
	}

}
