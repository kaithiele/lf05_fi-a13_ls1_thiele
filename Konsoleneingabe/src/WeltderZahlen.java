/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.09.2021
  * @author << SporeSee >>
  */

public class WeltderZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8 ;
    
    // Anzahl der Sterne in unserer Milchstra�e
    String anzahlSterne = "100 - 400 Milliarden" ;
    
    // Wie viele Einwohner hat Berlin?
     String bewohnerBerlin = "3,65 Millionen (2019)" ;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
      int alterTage = 7831 ;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
       int gewichtKilogramm = 150000 ;
    
    // Schreiben Sie auf, wie viele km� das gr��te Land der Erde hat?
       String flaecheGroessteLand = "17,1 Millionen km�" ;
    
    // Wie gro� ist das kleinste Land der Erde?
    
       String flaecheKleinsteLand = "0,44 km�" ;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Wieviele leben in Berlin?: " + bewohnerBerlin);
    
    System.out.println("Wieviele Tage bin ich alt?: " + alterTage);
    
    System.out.println("Wieviel wiegt das schwerste Tier? (in Kg): " + gewichtKilogramm);
    
    System.out.println("Wieviel km� hat das groesste Land?: " + flaecheGroessteLand);
    
    System.out.println("Wieviel km� hat das kleinste Land?: " + flaecheKleinsteLand);
    
    System.out.println("\n *******  Ende des Programms  *******");
    
  }
}