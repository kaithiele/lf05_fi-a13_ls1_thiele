
public class Aufgabe2_2 {

	public static void main(String[] args) {
		int a = 0;
		int b = 1;
		int c = 2;
		int d = 3;
		int e = 4;
		int f = 5;
		String g = "=";
		String h ="1";
		int i = 6;
		int j = 24;
		int k = 120;
		
		System.out.printf("%s!", a );
		System.out.printf("%5s", g );
		System.out.printf("%21s", g );
		System.out.printf("%4s\n", b );

		System.out.printf("%s!", b );
		System.out.printf("%5s 1", g );
		System.out.printf("%19s", g );
		System.out.printf("%4s\n", b );
		
		System.out.printf("%s!", c );
		System.out.printf("%5s 1 * 2", g );
		System.out.printf("%15s", g );
		System.out.printf("%4s\n", c );
		
		System.out.printf("%s!", d );
		System.out.printf("%5s 1 * 2 * 3", g );
		System.out.printf("%11s", g );
		System.out.printf("%4s\n", i );
		
		System.out.printf("%s!", e );
		System.out.printf("%5s 1 * 2 * 3 * 4", g );
		System.out.printf("%7s", g );
		System.out.printf("%4s\n", j );
		
		System.out.printf("%s!", f );
		System.out.printf("%5s 1 * 2 * 3 * 4 * 5", g );
		System.out.printf("%3s", g );
		System.out.printf("%4s\n", k );
		
	}

}
