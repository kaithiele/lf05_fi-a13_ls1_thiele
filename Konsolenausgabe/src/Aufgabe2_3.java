
public class Aufgabe2_3 {

	public static void main(String[] args) {
		int a = 20;
		int b = 10;
		String c = "+0";
		double d = 28.8889;
		double e = 23.3333;
		double f = 17.7778;
		double g = 6.6667;
		double h = 1.1111;
		int i = 30;
		
		String j = "Fahrenheit";
		String k = "Celsius";
		String l = "|";
		String m = "+20";
		String n = "+30";
		
		// Deffinierung der Variablen
		
		System.out.printf("%s", j );
		System.out.printf("%5s", l );
		System.out.printf("%11s\n", k );
		
		System.out.println("---------------------------");
		
		System.out.printf("%s", -a );
		System.out.printf("%12s", l );
		System.out.printf("%11.2f\n", -d );
		
		System.out.printf("%s", -b );
		System.out.printf("%12s", l );
		System.out.printf("%11.2f\n", -e );
		
		System.out.printf("%s", c );
		System.out.printf("%13s", l );
		System.out.printf("%11.2f\n", -f );
		
		System.out.printf("%s", m );
		System.out.printf("%12s", l );
		System.out.printf("%11.2f\n", -g );
		
		System.out.printf("%s", n );
		System.out.printf("%12s", l );
		System.out.printf("%11.2f\n", -h );

	}

}
