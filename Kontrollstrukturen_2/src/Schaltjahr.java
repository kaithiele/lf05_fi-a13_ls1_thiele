import java.util.Scanner;
public class Schaltjahr {
	public static void main(String[] args) {
		Scanner my_scanner = new Scanner(System.in);
		
		System.out.println("Bitte gebe ein Jahr an:\n");
		int year = my_scanner.nextInt();
		boolean schaltjahr = true;
		
		if (year % 4 == 0) {
			schaltjahr = true;
		} else if (schaltjahr && year % 100 == 0) {
			schaltjahr = false;
		} else if (!schaltjahr && year % 400 == 0) {
			schaltjahr = true;
		} else {
			schaltjahr = false;
		}
		
		if (schaltjahr) {
			System.out.println("\nDas Jahr " + year + " ist ein Schaltjahr.");
		} else {
			System.out.println("\nDas Jahr " + year + " ist kein Schaltjahr.");
		}
		
		my_scanner.close();
	}
}
