import java.util.Scanner;
public class Matrix {
	public static void main(String[] args) {
		Scanner my_scanner = new Scanner(System.in);
		//Zahl einlesen
		System.out.println("Bitte gebe die Zahl ein die nicht in der Matrix vorkommen soll[0-9]:");
		int number = my_scanner.nextInt();
		System.out.println("");
		
		for (int i = 0; i < 100; i++) {
			//Test auf eingelesene Zahl
			if (i == number || i % number == 0 || qsumme(i) == number || hatZahl(i, number)) {
				//Null in der Matrix lassen
				if (i != 0) System.out.print(" *\t");
				else System.out.print(" " + i + "\t");
			} else {
				//Formatierung und ausgeben von i
				if (i < 10) {
					System.out.print(" " + i + "\t");
				} else {
					System.out.print(i + "\t");
				}
			}
			//Formatierung
			if (i == 9 || i == 19 || i == 29 || i == 39 || i == 49 || i == 59 || i == 69 || i == 79 || i == 89) System.out.println("");
		}
		
		my_scanner.close();
	}
	
	//Test auf Quersumme der Zahl
	public static int qsumme(int x) {
		if (x <= 9) return x;
		
		int y = 0;
		while (x > 0) {
	        y += x % 10;
	        x /= 10;
	    }
		return y;
	}
	
	//Test auf Zahl in der Zahl
	public static boolean hatZahl(int x, int number)
	{
	    while (x != 0)
	    {
	        int y = x % 10;
	        if (y == number) return true;
	        x /= 10;
	    }
	    return false;
	}
}
