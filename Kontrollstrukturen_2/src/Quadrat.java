import java.util.Scanner;
public class Quadrat {
	public static void main(String[] args) {
		Scanner my_scanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie die Seitenlaenge des Quadrates an:");
		int a = my_scanner.nextInt();
		
		for (int i = 1; i <= a; i++) {
			if (i == 1 || i == a) {
				for (int j = 0; j < a; j++) {
					System.out.print("* ");
				}
				System.out.println("");
			} else {
				System.out.print("*");
				for (int f = 0; f < 2 * a - 3; f++) {
					System.out.print(" ");
				}
				System.out.println("*");
			}
		}
		
		my_scanner.close();
	}
}