﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	while (true) {
	    	double zuZahlenderBetrag = fahrkartenbestellungErfassen();
	    	double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	    	fahrkartenAusgeben();
	    	rueckgeldAusgeben(rückgabebetrag);
	
	    	System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.\n\n"+
	                          "--------------------------------------------------\n\n");
    	}
    }
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);

        double ticketEinzelpreis = 1.0;
        boolean running = false;

        do {
        	System.out.print("Bitte wählen Sie eine der folgenden Ticketarten:\n\nEinzelfahrschein Regeltarif AB [2,90 EUR] (1)\nTageskarte Regeltarif AB [8,60 EUR] (2)\nKleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n");
        	int auswahl = tastatur.nextInt();
        	
        	switch (auswahl) {
            case 1: ticketEinzelpreis = 2.9; running = false; break;
            case 2: ticketEinzelpreis = 8.6; running = false; break;
            case 3: ticketEinzelpreis = 23.5; running = false; break;
            default: 
            	System.out.println("\nGeben Sie eine Ganzzahl zwischen 1 und 3 ein.\n");
            	running = true;
            	break;
            }
        } while (running);
        
        System.out.print("Wie viele Tickets werden gekauft?: ");
        double anzahlTickets = tastatur.nextInt();
    	
    	return ticketEinzelpreis * anzahlTickets;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneMünze;
        
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   //System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.printf("%s%.2f EURO %s", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),"  ");
     	   System.out.print("\nEingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	
    	return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void warte (int time) {
    	try {
  			Thread.sleep(time);
  		} catch (InterruptedException e) {
  			e.printStackTrace();
  		}
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrscheine werden ausgegeben.");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    
    public static void muenzeAusgeben(double rückgabebetrag, String einheit) {
    	while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
        {
     	  System.out.println("2 " + einheit);
	          rückgabebetrag -= 2.0;
        }
        while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
        {
     	  System.out.println("1 " + einheit);
	          rückgabebetrag -= 1.0;
        }
        while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
        {
     	  System.out.println("0.50 " + einheit);
	          rückgabebetrag -= 0.5;
        }
        while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
        {
     	  System.out.println("0.20 " + einheit);
	          rückgabebetrag -= 0.2;
        }
        while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
        {
     	  System.out.println("0.10 " + einheit);
	          rückgabebetrag -= 0.1;
        }
        while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
        {
     	  System.out.println("0.05 " + einheit);
	          rückgabebetrag -= 0.05;
        }
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO", rückgabebetrag);
     	   System.out.println("\nwird in folgenden Münzen ausgezahlt:");
     	   muenzeAusgeben(rückgabebetrag, " EURO");
        }
    }
}