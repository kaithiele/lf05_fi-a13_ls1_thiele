import java.util.Scanner;

class FahrkartenautomatArray
{
    public static void main(String[] args)
    {
    	while (true) {
	    	double zuZahlenderBetrag = fahrkartenbestellungErfassen();
	    	double r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	    	fahrkartenAusgeben();
	    	rueckgeldAusgeben(r�ckgabebetrag);
	
	    	System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen eine gute Fahrt.\n\n"+
	                          "--------------------------------------------------\n\n");
    	}
    }
    // Vorteil ist: das wir eine bessere �bersicht erhalten und diese speziell f�r die einzelenen Bereiche anpassen k�nnen.
    // Dazu ist es Vorteilhaft, wodurch wir zuk�nftig alles einfach erg�nzen k�nnen.
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);

        double ticketEinzelpreis = 1.0;
        boolean running = true;
        
        double[] ticketpreise = {2.9, 3.3, 3.6, 1.9, 8.6, 9.0, 9.6, 23.5, 24.3, 24.9};
        String[] ticketnamen = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC",
        		"Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB",
        		"Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
        
        do {
        	System.out.println("Bitte w�hlen Sie eine der folgenden Ticketarten (Zahl w�hlen):\n");
        	
        	for (int i = 0; i < ticketpreise.length; i++) {
        		System.out.printf("(%s) %s kostet %.2f Euro\n", i, ticketnamen[i], ticketpreise[i]);
        	}
        	
        	System.out.println("Geben Sie -1 ein um das Programm zu beenden.\n");
        	int auswahl = tastatur.nextInt();
        	if (auswahl == -1) {
        		System.out.println("Auf Wiedersehen!");
        		System.exit(0);
        	}
        	
        	if (auswahl < 0 || auswahl > ticketpreise.length - 1) {
        		System.out.println("Geben Sie eine valide Nummer ein!\n");
        	} else {
        		running = false;
        		ticketEinzelpreis = ticketpreise[auswahl];
        	}
        } while (running);
        
        System.out.print("Wie viele Tickets werden gekauft?: ");
        double anzahlTickets = tastatur.nextInt();
    	
    	return ticketEinzelpreis * anzahlTickets;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneM�nze;
        
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   //System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.printf("%s%.2f EURO %s", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),"  ");
     	   System.out.print("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
    	
    	return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void warte (int time) {
    	try {
  			Thread.sleep(time);
  		} catch (InterruptedException e) {
  			e.printStackTrace();
  		}
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrscheine werden ausgegeben.");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    
    public static void muenzeAusgeben(double r�ckgabebetrag, String einheit) {
    	while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
        {
     	  System.out.println("2 " + einheit);
	          r�ckgabebetrag -= 2.0;
        }
        while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
        {
     	  System.out.println("1 " + einheit);
	          r�ckgabebetrag -= 1.0;
        }
        while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
        {
     	  System.out.println("0.50 " + einheit);
	          r�ckgabebetrag -= 0.5;
        }
        while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
        {
     	  System.out.println("0.20 " + einheit);
	          r�ckgabebetrag -= 0.2;
        }
        while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
        {
     	  System.out.println("0.10 " + einheit);
	          r�ckgabebetrag -= 0.1;
        }
        while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
        {
     	  System.out.println("0.05 " + einheit);
	          r�ckgabebetrag -= 0.05;
        }
    }
    
    public static void rueckgeldAusgeben(double r�ckgabebetrag) {
    	if(r�ckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO", r�ckgabebetrag);
     	   System.out.println("\nwird in folgenden M�nzen ausgezahlt:");
     	   muenzeAusgeben(r�ckgabebetrag, " EURO");
        }
    }
}