import java.util.Scanner;

class FahrkartenautomatTicket
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double r�ckgabebetrag;
       double einzelpreis;
       short anzahltickets;
       
       System.out.println("Wie viel kostet ein Ticket?");
       einzelpreis = tastatur.nextDouble();
      
       System.out.println("Wie viele Tickets m�chten Sie kaufen?");
       anzahltickets = tastatur.nextShort(); 
       
       zuZahlenderBetrag = einzelpreis * anzahltickets; 
       
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.00;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
    
       {
           System.out.printf("Noch zu zahlen: %.2f", + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
           System.out.print(" Euro \nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
           eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(r�ckgabebetrag > 0.00)
       {
           System.out.printf("Der R�ckgabebetrag in H�he von %.2f ",r�ckgabebetrag);
           System.out.println("� wird in folgenden M�nzen ausgezahlt:");

           while(r�ckgabebetrag >= 2.00) // 2 EURO-M�nzen
           {
              System.out.println("2.00 �");
              r�ckgabebetrag -= 2.00; 
           }
           while(r�ckgabebetrag >= 1.00) // 1 EURO-M�nzen
           {
              System.out.println("1.00 �");
              r�ckgabebetrag -= 1.00;
           }
           while(r�ckgabebetrag >= 0.50) // 50 CENT-M�nzen
           {
              System.out.println("50 CENT");
              r�ckgabebetrag -= 0.50;
           }
           while(r�ckgabebetrag >= 0.20) // 20 CENT-M�nzen
           {
              System.out.println("20 CENT");
               r�ckgabebetrag -= 0.20;
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
              System.out.println("10 CENT");
              r�ckgabebetrag -= 0.10;
           }
           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
              System.out.println("5 CENT");
               r�ckgabebetrag -= 0.05;
           }
       }
       
           System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
                                 "vor Fahrtantritt entwerten zu lassen!\n"+
                              "Wir w�nschen Ihnen eine gute Fahrt.");
           
           /* Ich habe mich f�r Short entschieden, da es f�r die Anzahl der Tickets ausreicht.
            * und bei dem Einzelpreis ist double selbsterkl�hrend um es einheitlich zu lassen, behalte ich es dabei.
            * Zu Punkt 6: In Zeile 22 wird berechnet den Einzelpreis x die Ticketanzahl.
            * Die darauffolgende Ausgabe ist unver�ndert geblieben. */
       
           }  
    }